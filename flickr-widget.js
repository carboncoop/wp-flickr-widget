jQuery(document).ready(function($) {
	var widgets = document.querySelectorAll('.flickr_images');

	widgets.forEach(function(widget) {
		var flickrid = widget.getAttribute('data-flickrid');
		var count = parseInt(widget.getAttribute('data-count'), 10);

		var url = "//api.flickr.com/services/feeds/photos_public.gne?ids=" + flickrid + "&lang=en-us&format=json&jsoncallback=?";

		$.getJSON(url, function(data) {
			$.each(data.items, function(index, item) {
				// Scale down
				var src = item.media.m.replace('_m.jpg', '_s.jpg');

				$('<img class="flickr">')
					.attr("src", src)
					.attr('alt', item.title)
					.attr('width', 81)
					.attr('height', 81)
					.appendTo($(widget))
					.wrap("<li><a href='" + item.link + "' class='flickr-img-link' target='_blank'></a></li>");

				return index+1<count;
			});
		});
	});
});
