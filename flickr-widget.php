<?php
/**
 * Plugin Name:     Flickr Widget
 * Plugin URI:      https://gitlab.com/carboncoop/wp-flickr-widget
 * Description:     Modified Swift Framework Flickr photos widget
 * Author:          Swift Framework / Anna Sidwell
 * Version:         0.1.0
 * GitLab Plugin URI: carboncoop/wp-flickr-widget
 */

class flickr_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'flickr-widget',
			'Flickr Widget',
			[
				'classname' => 'flickr-widget',
				'description' => 'Show off your favorite Flickr photos'
			]);
	}

	function form($instance) {

		$instance = wp_parse_args( (array) $instance, array('title' => 'Flickr Photos', 'number' => 6, 'flickr_api' => '', 'flickr_id' => '') );
		$title = esc_attr($instance['title']);
		$flickr_api = $instance['flickr_api'];
		$flickr_id = $instance['flickr_id'];
		$number = absint($instance['number']);

		$id_getter_url = "http://idgettr.com";

?>
	<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'swift-framework-admin');?>:</label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	</p>

	<p>
		<label for="<?php echo $this->get_field_id('flickr_id'); ?>"><?php _e('Flickr ID', 'swift-framework-admin');?>:</label>
		<input class="widefat" id="<?php echo $this->get_field_id('flickr_id'); ?>" name="<?php echo $this->get_field_name('flickr_id'); ?>" type="text" value="<?php echo $flickr_id; ?>" />
		<small>Don't know your ID? Head on over to <a href="<?php echo $id_getter_url; ?>">idgettr</a> to find it.</small>
	</p>

	<p>
		<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Photos', 'swift-framework-admin');?>:</label>
		<input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
	</p>

<?php
	}

	function update($new_instance, $old_instance) {

		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['flickr_api']=$new_instance['flickr_api'];
		$instance['flickr_id']=$new_instance['flickr_id'];
		$instance['number']=$new_instance['number'];

		return $instance;
	}

	function widget($args, $instance) {
		extract($args);

		$title = apply_filters('widget_title', $instance['title']);
		$flickrid = $instance['flickr_id'];
		$count = (int) $instance['number'];

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		echo $before_widget;
		echo "<ul class='flickr_images clearfix' " .
				"data-flickrid='" . $flickrid . "' " .
				"data-count='" . $count . "' " .
				"></ul>";
		echo $after_widget;

		$this->load_script();
	}

	function load_script() {
		wp_enqueue_script('flickr_widget_script');
	}
}

/* Add the script, yay */
function register_flickr_script() {
	wp_register_script(
		'flickr_widget_script',
		plugin_dir_url( __FILE__ ) . 'flickr-widget.js',
		[ 'jquery' ],
		null,
		true
	);
}

add_action('wp_enqueue_scripts', 'register_flickr_script');

/* Low priority means we override the Dante theme's built in version */
add_action('widgets_init', 'load_flickr_widget', 100);

function load_flickr_widget() {
	register_widget('flickr_widget');
}

?>
